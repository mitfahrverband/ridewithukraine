from ast import parse
import datetime,json,calendar,os
from jinja2 import Environment, FileSystemLoader
import Carpool
from pydantic import parse_obj_as
from typing import List
from datetime import timedelta


#Global variables
weekdaysLanguages = {
    "en": {
        "monday": "Monday",
        "tuesday": "Tuesday",
        "wednesday": "Wednesday",
        "thursday": "Thursday",
        "friday": "Friday",
        "saturday": "Saturday",
        "sunday": "Sunday"
    },
    "de": {
        "monday": "Montag",
        "tuesday": "Dienstag",
        "wednesday": "Mittwoch",
        "thursday": "Donnerstag",
        "friday": "Freitag",
        "saturday": "Samstag",
        "sunday": "Sonntag"
    },
    "ro": {
        "monday": "Luni",
        "tuesday": "Marți",
        "wednesday": "Miercuri",
        "thursday": "Joi",
        "friday": "Vineri",
        "saturday": "Sâmbătă",
        "sunday": "Duminică"
    },
    "hu": {
        "monday": "Hétfő",
        "tuesday": "Kedd",
        "wednesday": "Szerda",
        "thursday": "Csütörtök",
        "friday": "Péntek",
        "saturday": "Szombat",
        "sunday": "Vasárnap"
    },
    "sk": {
        "monday": "Pondelok",
        "tuesday": "Utorok",
        "wednesday": "Streda",
        "thursday": "Štvrtok",
        "friday": "Piatok",
        "saturday": "Sobota",
        "sunday": "Nedeľa"
    },
    "pl": {
        "monday": "Poniedziałek",
        "tuesday": "Wtorek",
        "wednesday": "Środa",
        "thursday": "Czwartek",
        "friday": "Piątek",
        "saturday": "Sobota",
        "sunday": "Niedziela"
    },
    "fr": {
        "monday": "Lundi",
        "tuesday": "Mardi",
        "wednesday": "Mercredi",
        "thursday": "Jeudi",
        "friday": "Vendredi",
        "saturday": "Samedi",
        "sunday": "Dimanche"
    },
    "ru": {
        "monday": "Понедельник",
        "tuesday": "Вторник",
        "wednesday": "Среда",
        "thursday": "Четверг",
        "friday": "Пятница",
        "saturday": "Суббота",
        "sunday": "Воскресенье"
    },
    "ua": {
        "monday": "Понеділок",
        "tuesday": "Вівторок",
        "wednesday": "Середа",
        "thursday": "Четвер",
        "friday": "П’ятниця",
        "saturday": "Субота",
        "sunday": "Неділя"
    },
}

weekdayList = {"monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"}

currTime = datetime.datetime.now().time()

today = datetime.datetime.today()

weekdayToday = calendar.day_name[today.weekday()].lower()

listOfLanguages = ["en", "de", "ro", "hu", "ru", "ua", "sk", "fr", "pl"]

# Functions

#Loads json file to dictionary with the help of Carpool model
def loadFile(path: str):
    trips = list()
    with open(path, "r") as d:
        jsondata = json.load(d)
    for item in jsondata:
        trips.append(Carpool.Carpool.parse_obj(item))
    return trips

#Sets the time that will be rendered
def setTime(trip: Carpool.Carpool):
        if trip.departTime != None:
            trip.departTimeRender = trip.departTime.strftime("%H:%M")


#Finds the next weekday on which given trip occurs
def findNextWeekDay(trip, language):
    i = 1
    while i <= 7:
        tmpDay = calendar.day_name[(today+timedelta(days=i)).weekday()].lower()
        if tmpDay in trip.weekdays:
            trip.departDateRender = weekdaysLanguages[language][tmpDay] 
            break
        i += 1


#Attahes the correct logo to a trip(with giving the relative filepath to the logo)
def attachLogo(trip):
    if "mifaz" in trip.deeplink:
            trip.logo = "../img/mifaz.png"
    elif "bessermitfahren" in trip.deeplink:
            trip.logo = "../img/bessermitfahren_logo.png"
    else: trip.logo = "../img/icon_ride2go_green_small.jpg"


#Checks if departTime is the type of time. If not, it creates it from the given datetime
def checkDepartTimeType(trip):
    if(type(trip.departTime) != datetime.time and trip.departTime != None):
                tripTime = trip.departTime.time()
    else:
        tripTime = trip.departTime
    return tripTime


#Checks if the departTime is datetime type, and if it is, sets the date which will be rendered
def checkIfTimeIsDate(trip):
    if type(trip.departTime) == datetime.datetime:
        trip.departDateRender = trip.departTime.strftime("%d.%m.")


#Renders the html file from the template
def renderFile(language, templatesFolder, templateFile, tripList, extension):
    fileloader = FileSystemLoader(templatesFolder)
    env = Environment(loader=fileloader)
    rendered = env.get_template(templateFile).render(list = tripList,title=language, weekdays = weekdayList, lang = language)
    fileName = f"triplist_{language}.{extension}"
    with open(f"./htmls/{fileName}", "w") as f:
        f.write(rendered)

isExist = os.path.exists("./htmls")

if not isExist:
    os.makedirs("./htmls")

#Main loop for all the languages
for language in listOfLanguages:
    tripList = loadFile(f"./json_data/json_data.json")

    #setting the data, that will be sent to the template, and will be rendered
    for trip in tripList:
        checkIfTimeIsDate(trip)
        setTime(trip)
        if trip.departDate != None:
            trip.departDateRender = trip.departDate.strftime("%d.%m.")
        else:
            tripTime = checkDepartTimeType(trip)
            if trip.weekdays != None:
                if tripTime > currTime:
                    if weekdayToday in trip.weekdays:
                        trip.departDateRender = weekdaysLanguages[language][weekdayToday] 
                    else:
                        findNextWeekDay(trip, language)
                else:
                    findNextWeekDay(trip, language)
        attachLogo(trip)


    #This part renders the html and writes it in a file
    renderFile(language, "templates", "template.html", tripList, "html")