How to use:
    -run the "render_and_upload_htmls.sh" bash script and it will take care of everything:
        -removes the previous json file (if existed)
        -downloads the latest json_data.json from the server
        -renders the html files based on the new data
        -uploads the new html files to the server
    -run only if you are in the "render" directory

Everything will be happening inside this directory, the generated htmls are in the "htmls" directory, the new json file will be
downloaded to the "json_data" directory.

The render_html.py is what renders the new htmls, and the Carpool.py contains the pydantic model to parse the json.
The "templates" directory contains the Jinja2 template, and the __pycache__ directory should contain any external dependencies for the python script to run.