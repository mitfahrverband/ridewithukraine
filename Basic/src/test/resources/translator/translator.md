## [Translator Test](-)

In this test the locations in the first column are translated to their english equivalents(if there are any)
in the secon column.

| [](- "#response=translateCity(#city)") [Original City](- "#city") | [Translated City](- "?=#response") |
|-------------------------------------------------------------------|------------------------------------|
| München                                                           | Munich                             |
| Berlin                                                            | Berlin                             |
| Würzburg                                                          | Würzburg                           |
| Kecskemét                                                         | Kecskemét                          |
| Hajdúszoboszló                                                    | Hajdúszoboszló                     |
| Vác                                                               | Vác                                |
| Bräunlingen                                                       | Bräunlingen                        |
| Klagenfurt am Wörthersee                                          | Klagenfurt am Wörthersee           |
| Köln                                                              | Cologne                            |


