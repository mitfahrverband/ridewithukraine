package translator


import org.concordion.integration.junit4.ConcordionRunner
import org.junit.runner.RunWith
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL

@RunWith(ConcordionRunner::class)

class translatorTest{

    fun translateCity(originalCity:String): String {
        val orCity = originalCity.trim()
        var saveTranslatedCityName = ""
        val cityNameForURL = java.net.URLEncoder.encode(orCity)
        val languageSelect = "en"
        val url = URL("http://api.geonames.org/search?name_equals=$cityNameForURL&lang=$languageSelect&maxRows=1&username=mfdz")
        val connection = url.openConnection()
        BufferedReader(InputStreamReader(connection.getInputStream())).use { inp ->
            var line: String?
            while (inp.readLine().also { line = it } != null) {
                if(line!!.contains("<name>")){
                    saveTranslatedCityName = line!!.substring(14, line!!.length-7)
                    break
                }
            }
        }
        return saveTranslatedCityName
    }
}

