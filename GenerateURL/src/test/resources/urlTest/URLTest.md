


All URL starts with rwu.org/ - depricated for the sake of simplicity

| [From]()  | [To]()    | [Date]()     | [Time]() | [URL]()                        |
|-----------|-----------|--------------|----------|--------------------------------|
| Berlin    | München   | September 12 | 12 a.m   | Ber-Mun/-/Sep/12/12AM          |
| Stuttgart | Köln      | July 27      | 1 p.m    | Stu-Col/-/Jul/27/1PM           | -> e.g this has no other stops
| Stuttgart | Köln      | July 27      | 1 p.m    | Stu-Col/other-stops/Jul/27/1PM | -> e.g this has more stops
| Berlin    | München   | August 10    | 1 p.m    | Ber-Mun/other-stops/Aug/10/1PM |
| München   | Budapest  | July 27      | 1 p.m    | Mun-Bud/other-stops/Jul/27/1PM | 
| Stuttgart | Köln      | July 27      | 1 p.m    | Stu-Col/-/Jul/27/1PM           | -> e.g this has no other stops either ? not certain what to do



