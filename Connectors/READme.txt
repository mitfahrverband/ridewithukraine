To use the notebook:
    -create and start a new virtual environment
    -pip install requirements.txt
    -open jupyter lab
    -put the key into a 'key.txt' file from keypass. It will be loaded from that file
    -the locations.json contains the locations that serve as origin and destination for the trip searches