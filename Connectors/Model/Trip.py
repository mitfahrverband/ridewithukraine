from pydantic import BaseModel, Field, HttpUrl
from datetime import datetime, time, date
from typing import Optional, Union, List
from enum import Enum

class Weekdays(str, Enum):
    monday = "monday"
    tuesday = "tuesday"
    wednesday = "wednesday"
    thursday = "thursday"
    friday = "friday"
    saturday = "saturday"
    sunday = "sunday"

    
class Coordinates(BaseModel):
    lat: float = Field(
        ge=-90,
        lt=90
    )
    lon: float = Field(
        ge=-180,
        lt=180
    )
        

class Stop(BaseModel):
    address: str
    coordinates: Coordinates
        
        
class Trip(BaseModel):
    lastUpdated: Optional[datetime]
    departTime: Union[time, datetime, None]
    departDate: Optional[date]
    weekdays: Optional[List[Weekdays]]
    stops: List[Stop]
    deeplink: HttpUrl