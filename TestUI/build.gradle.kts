import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.6.21"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation ("org.jetbrains.kotlin:kotlin-stdlib")
    testImplementation("org.concordion:concordion:3.1.3")


    testImplementation("org.concordion:concordion-screenshot-extension:1.3.0")
    testImplementation("org.concordion:concordion-storyboard-extension:2.0.2")
    testImplementation("org.concordion:concordion-timing-extension:1.1.0")
    testImplementation("org.slf4j:slf4j-api:1.7.36")

    implementation ("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.1")
    implementation("org.json:json:20220320")
    implementation ("org.seleniumhq.selenium:selenium-java:3.141.59")
    implementation ("org.seleniumhq.selenium:selenium-chrome-driver:3.141.59")
}