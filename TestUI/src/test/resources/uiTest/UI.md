# [UI Test](-)

UI test for [ridewithukraie](https://ridewithukraine.org/index-roli.php)

When you open up the page, the language should be [English](- "c:assert-true=testIndexpage()")

On the page there are more then [50](- "c:assert-true=tripNumberCheck(#TEXT)") trips

There are a total of [](- "c:echo=tripNumber") trips

Now we have [scrolled down to the bottom](- "c:assert-true=scrollDown()") of the page

Now we [click the language menu](- "c:assert-true=clickLangMenu()")

The table below shows the results of every language change:

| [](- "#response=changeLang(#lang)") [Language I want to change to](- "#lang") | [Expected result](- "?=#response") |
|-------------------------------------------------------------------------------|------------------------------------|
| ua                                                                            | ua                                 |
| ru                                                                            | ru                                 |
| de                                                                            | de                                 |
| en                                                                            | en                                 |
| fr                                                                            | fr                                 |
| pl                                                                            | pl                                 |
| sk                                                                            | sk                                 |
| hu                                                                            | hu                                 |
| ro                                                                            | ro                                 |


