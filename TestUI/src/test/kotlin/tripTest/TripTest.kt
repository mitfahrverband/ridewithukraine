package tripTest

import org.concordion.api.AfterSuite
import org.concordion.api.extension.Extension
import org.concordion.api.option.ConcordionOptions
import org.concordion.ext.StoryboardExtension
import org.concordion.integration.junit4.ConcordionRunner
import org.junit.runner.RunWith
import org.openqa.selenium.*
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import seleniumScreenshotTaker.Browser
import seleniumScreenshotTaker.SeleniumScreenshotTaker
import java.lang.Thread.sleep

@RunWith(ConcordionRunner::class)
@ConcordionOptions(declareNamespaces = ["ext", "urn:concordion-extensions:2010"])

class TripTest {
    var browser = Browser()
    var driver: WebDriver = browser.driver
    var wait = WebDriverWait(driver, 30)
    var js = driver as JavascriptExecutor

    @Extension
    private val storyBoard = StoryboardExtension()
    private val screenshotTaker = SeleniumScreenshotTaker(browser)

    init{
        driver.get("https://ridewithukraine.org")
        storyBoard.setScreenshotTaker(screenshotTaker)
    }

    fun getElement(cssSelector: String): WebElement{
        var element = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(cssSelector)))
        js.executeScript("arguments[0].scrollIntoView()", element)
        sleep(1000)
        return element
    }

    fun clickRideAlong(){
        var element = getElement("input[value='searching']")
        sleep(1000)
        element.click()
        storyBoard.addScreenshot("Clicked on Ride along", "")
    }
    fun clickTakeAlong(){
        var element = getElement("input[value='driving']")
        sleep(1000)
        element.click()
        storyBoard.addScreenshot("Clicked on Take along", "")
    }

    fun clickNowDate(){
        var element = getElement("#step2 .actions button[type='button']")
        sleep(1000)
        element.click()
        storyBoard.addScreenshot("Enter depart date", "Clicked on the now button")
    }

    fun enterStartPlace(){
        var element = getElement("input[name='departureLocation']")
        sleep(1000)
        element.sendKeys("munich")
        sleep(1000)
        storyBoard.addScreenshot("Enter text for depart place", "")
        var element2 = driver.findElement(By.xpath("//div[@id='step3']/div[@class='actions']/div/p"))
        element2.click()
        sleep(1500)
        storyBoard.addScreenshot("Autocomplete clicked", "")
    }

    fun enterDestination(){
        var element = getElement("input[name='destination']")
        sleep(1000)
        element.sendKeys("berlin")
        sleep(1000)
        storyBoard.addScreenshot("Enter text for destination","")
        var element2 = driver.findElement(By.xpath("//div[@id='step4']/div[@class='actions']/div/p"))
        element2.click()
        sleep(1500)
        storyBoard.addScreenshot("Autocomplete clicked", "")
    }

    fun clearStep5(){
        var element = getElement("input[name='mail']")
        element.sendKeys("for.test.rwu@gmail.com")
    }

    fun clickSend(){
        var element = getElement("#step5 .actions button")
        sleep(1000)
        element.click()
        storyBoard.addScreenshot("The page after clicking the send button","")
    }

    fun clickOk(){
        var element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='success-modal']/div/a")))
        storyBoard.addScreenshot("Success modal", "")
        element.click()
    }

    fun testRideAlong(){
        var element = driver.findElement(By.className("header-top"))
        js.executeScript("arguments[0].scrollIntoView()", element)
        sleep(1000)
        storyBoard.addScreenshot("The page when it is loaded", "")
        clickRideAlong()
        clickNowDate()
        enterStartPlace()
        enterDestination()
        clickSend()
    }

    fun testTakeAlong(){
        var element = driver.findElement(By.className("header-top"))
        js.executeScript("arguments[0].scrollIntoView()", element)
        sleep(1000)
        storyBoard.addScreenshot("The page when it is loaded", "")
        clickTakeAlong()
        clickNowDate()
        enterStartPlace()
        enterDestination()
        clearStep5()
        clickSend()
        sleep(5000)
        clickOk()
        sleep(10000)
        deleteTrip()
    }


    fun deleteTrip(){
        driver.get("https://gmail.com")
        var mail = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("input[name='identifier']")))
        mail.sendKeys("for.test.rwu@gmail.com")
        mail.sendKeys(Keys.ENTER)
        sleep(2000)
        var pwd = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("input[type='password']")))
        pwd.sendKeys("testforrwu")
        pwd.sendKeys(Keys.ENTER)
        sleep(5000)
        driver.findElement(By.xpath("//span[text()='New offer was created']")).findElement(By.xpath("./../..")).click()
        sleep(2000)
        driver.findElement(By.partialLinkText("delete/?tripId=")).click()
        sleep(2000)
    }

    @AfterSuite
    fun close(){
        driver.quit()
    }


}