package seleniumScreenshotTaker

import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.support.events.EventFiringWebDriver

/**
 * Manages the browser session.
 */
class Browser {
    var driver: WebDriver
        private set

    init {
        val options = ChromeOptions()
        if ("true" == System.getenv("HEADLESS_CHROME")) {
            options.addArguments("--headless")
            options.addArguments("--no-sandbox")
            options.addArguments("--disable-dev-shm-usage")
        }
        driver = getLocalDriver()
        val efwd = EventFiringWebDriver(driver)
        driver = efwd
    }

    fun close() {
        driver.quit()
    }

}

fun getLocalDriver(): WebDriver{
    System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver/chromedriver")
    return ChromeDriver()
}