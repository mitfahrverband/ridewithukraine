package seleniumScreenshotTaker

import org.concordion.ext.ScreenshotTaker
import org.concordion.ext.ScreenshotUnavailableException
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.events.EventFiringWebDriver
import seleniumScreenshotTaker.Browser
import java.awt.Dimension
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import javax.imageio.ImageIO

/**
 * Takes screenshots of the system under test.
 */
class SeleniumScreenshotTaker(browser: Browser) : ScreenshotTaker {
    private val driver: WebDriver?

    init {
        var baseDriver = browser.driver
        while (baseDriver is EventFiringWebDriver) {
            baseDriver = baseDriver.wrappedDriver
        }
        driver = baseDriver
    }

    @Throws(IOException::class)
    override fun writeScreenshotTo(outputStream: OutputStream): Dimension {
        val screenshot: ByteArray
        screenshot = try {
            (driver as TakesScreenshot?)!!.getScreenshotAs(OutputType.BYTES)
        } catch (e: ClassCastException) {
            throw ScreenshotUnavailableException("driver does not implement TakesScreenshot")
        }
        outputStream.write(screenshot)
        return getImageDimension(screenshot)
    }

    @Throws(IOException::class)
    private fun getImageDimension(screenshot: ByteArray): Dimension {
        val `in`: InputStream = ByteArrayInputStream(screenshot)
        val buf = ImageIO.read(`in`)
        return Dimension(buf.width, buf.height)
    }

    override fun getFileExtension(): String {
        return "png"
    }
}