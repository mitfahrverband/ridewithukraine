package uiTest

import org.concordion.api.AfterSuite
import org.concordion.api.extension.Extension
import org.concordion.api.option.ConcordionOptions
import org.concordion.ext.StoryboardExtension
import org.concordion.integration.junit4.ConcordionRunner
import org.junit.runner.RunWith
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import seleniumScreenshotTaker.Browser
import seleniumScreenshotTaker.SeleniumScreenshotTaker
import java.lang.Thread.sleep
import java.util.concurrent.TimeUnit

@RunWith(ConcordionRunner::class)
@ConcordionOptions(declareNamespaces = ["ext", "urn:concordion-extensions:2010"])

class UITest {


    private var browser: Browser = Browser()
    private var driver: WebDriver


    @Extension
    private val storyBoard = StoryboardExtension()
    private val screenshotTaker = SeleniumScreenshotTaker(browser)

    init {
        driver = browser.driver
        driver.get("https://ridewithukraine.org/index-roli.php")
        storyBoard.setScreenshotTaker(screenshotTaker)
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS)
    }
    var js = driver as JavascriptExecutor


    fun testIndexpage(): Boolean{
        storyBoard.addScreenshot("The page when loaded", "This is how the page looks like, when it is loaded")
        val element = driver.findElement(By.tagName("html"))
        val sample = driver.findElement(By.className("heading")).text
        val elementLang = element.getAttribute("lang")
        if(elementLang == "en" && sample.contains("5 steps")) return true
        return false
    }

    fun changeLang(language: String): String{
        var langMenu = driver.findElement(By.id("lang-btn"))
        js.executeScript("arguments[0].scrollIntoView()", langMenu)
        sleep(1000)
        langMenu.click()

        var element = driver.findElement(By.partialLinkText(language.uppercase().trim()))

        element.click()
        storyBoard.addScreenshot("Switching to $language", "")
        var switchedLang = driver.findElement(By.tagName("html")).getAttribute("lang")

        return switchedLang
    }

    fun clickLangMenu(): Boolean{
        var element = driver.findElement(By.id("lang-btn"))
        js.executeScript("arguments[0].scrollIntoView()", element)
        sleep(1000)
        element.click()
        storyBoard.addScreenshot("Language menu", "This is how the language menu looks like")
        element = driver.findElement(By.partialLinkText("DE - "))
        element.click()

        if(element != null){
            return true
        }
        return false
    }

    fun tripNumberCheck(numberOfTrips: Int): Boolean{
        var trips = driver.findElements(By.className("item"))
        if(trips.size >= numberOfTrips){
            return true
        }
        return false
    }

    fun tripNumber(): Int{
        var trips = driver.findElements(By.className("item"))

        return trips.size
    }

    fun scrollDown(): Boolean{
        var element = driver.findElement(By.className("footer"))
        js.executeScript("arguments[0].scrollIntoView()", element)
        sleep(1500)
        storyBoard.addScreenshot("Scrolled down", "This is how the bottom of the page looks like")
        if(element != null) return true
        return false
    }


    @AfterSuite
    fun close(){
        driver.quit()
    }

}
